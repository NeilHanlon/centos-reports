## [SIG Name](https://wiki.centos.org/SpecialInterestGroup/SigName/)

> We'd like each SIG to have a short blurb about what they do.

The Whatever SIG does something that we can describe in one or two short sentences.

> If you have more than a few bullet points of information, we
> encourage you to write a full report as a separate blog post.
> But still, we'd like bullet point highlights below.

Read the full [Whatever SIG report](https://blog.centos.org/the-full-report/).

> Now tell us about what's going on in your SIG. Keep the bullet
> points short and snappy. If there's a lot of information, it
> should go somewhere else, and the you should link to it.

* Did somebody speak somewhere? Or does somebody have an upcoming speaking slot?

* Note if there are membership or leadership changes.

* Tell us about something awesome you did besides packaging.

* Did you improve your docs, or your build pipeline, or something else? Tell us.

* Package [XYZ](https://package.uri/) has been updated to version x.y.z. This provides amazing new feature X.

* Package [XYZ](https://package.uri/) has been updated to version x.y.z. This fixes glaring security flaw Y.

* Package [XYZ](https://package.uri/) has been updated to version x.y.z.

